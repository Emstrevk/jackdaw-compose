#[allow(unused_imports)] use jackdaw_lib::note::Note;
#[allow(unused_imports)] use jackdaw_lib::score::Score;
#[allow(unused_imports)] use jackdaw_lib::tone::*;

use jackdaw_lib::note_strings::parse_notes;
use jackdaw_lib::toml_compose::*;
use jackdaw_lib::jackdaw_client::*;

use std::{time, thread};
use std::collections::HashMap;

use log::{info, warn, debug, error};
#[macro_use] extern crate log;
use log::Level;

#[tokio::main]
pub async fn compose_loop() {

    // Change logging level by e.g. "RUST_LOG=info cargo run"
    env_logger::init();

    let interval = time::Duration::from_millis(300);

    println!("Reading live from yml, Ctrl+c to exit");
    let mut last_contents = "".to_string();
    loop {

        let contents = std::fs::read_to_string("compose.toml").unwrap();

        if contents != last_contents {
            last_contents = contents.to_string();
            let composition = parse(&contents).unwrap();

            for (name, score) in composition {
                prepare(score.notes().clone(), &name);
            }

            force_reset();
            send_batch();
        }

        thread::sleep(interval);
    };
}

pub fn main() -> () {
    compose_loop();
}
